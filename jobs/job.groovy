String baseDir = "my-checks"
String gitUrl = "https://gitlab.com/angelin/apache-commons-io.git"

folder(baseDir){
    description "This is basic dir for jobs"
}

job("$baseDir/compile-test"){
    scm {
        git {
            remote {
                url(gitUrl)
                branch("master")
            }
        }
    }
  
    customWorkspace('/tmp/workspace')

    steps {
        maven('-e clean test')
    }
    
    triggers {
        scm("*/1 * * * *")
    }

    publishers {
        extendedEmail {
            recipientList('andreyangelin01@gmail.com')
            defaultSubject('Oops')
            defaultContent('Something broken')
            contentType('text/html')
            triggers {
                failure()
                stillUnstable {
                    subject('Subject')
                    content('Body')
                    sendTo {
                        developers()
                        requester()
                        culprits()
                    }
                }
            }
        }
    }
}
